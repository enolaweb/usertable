<?php

namespace App\Http\Controllers;


use App\User;

class UserController
{
    public function index() {
        return User::orderBy(request('sort', 'id'), request('order', 'DESC'))
            ->where('name', 'like', '%' . request('name') . '%')
            ->paginate(request('per_page', 10));
    }

    public function indexView() {
        return view('users');
    }
}