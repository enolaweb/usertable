const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)  {
    mix.sass('app.scss', 'public/assets/css')
        .styles([
            'public/assets/css/app.css',
            'node_modules/ng-table/dist/ng-table.min.css'
        ], 'public/assets/css/', './')
        .scripts([
            'node_modules/angular/angular.min.js',
            'node_modules/underscore/underscore-min.js',
            'node_modules/restangular/dist/restangular.min.js',
            'node_modules/ng-table/dist/ng-table.min.js',
            'resources/assets/js/app.js',
            'resources/assets/js/components/**'
        ], 'public/assets/js', './')
       .version([
           'assets/js/all.js',
           'assets/css/all.css'
       ]);
});
