(function() {
    'use strict';
    angular.module('table', [])
        .factory('table', table);

    table.$inject = ['NgTableParams'];

    function table(NgTableParams) {
        return {
            create: function (params) {
                var parseParams = function (params) {
                    var postParams = {
                        page: params.page(),
                        per_page: params.count()
                    };
                    var sorting = params.sorting();
                    if (Object.keys(sorting).length > 0) {
                        var key = Object.keys(sorting)[0];
                        postParams.sort = key;
                        postParams['order'] = sorting[key]
                    }
                    var filters = params.filter();
                    for (var i in filters) {
                        postParams[i] = filters[i];
                    }
                    return postParams;
                };

                return new NgTableParams({}, {
                    getData: function (tableParams) {
                        return params.get(parseParams(tableParams)).then(function (result) {
                            tableParams.total(result.meta.total);
                            tableParams.page(result.meta.current_page);
                            return result;
                        });
                    }
                });
            }
        }
    }
})();