var app = angular
    .module('app', [
        'restangular',
        'table',
        'ngTable'
    ]);

app.config(['RestangularProvider', function(RestangularProvider) {
    RestangularProvider.setResponseExtractor(function extractResponse(serverResponse, operation) {
        if ('data' in serverResponse) {
            var newResponse = serverResponse.data;
            newResponse.meta = {
                total: serverResponse.total,
                per_page: serverResponse.per_page,
                current_page: serverResponse.current_page,
                last_page: serverResponse.last_page
            };
        }
        return newResponse;
    });
}]);