<!DOCTYPE html>
<html lang="en">
<head>
    <link href="{{ elixir("assets/css/all.css") }}" rel="stylesheet" type="text/css" />
</head>
<body ng-app="app">
    <div ng-controller="UserController as UserCtrl">
        <div class="panel-body table-responsive">
            <table ng-cloak ng-table="UserCtrl.userTable" class="table table-striped table-hover">
                <tr ng-repeat="user in $data">
                    <td data-title="'ID'" sortable="'id'" ng-bind="user.id"></td>
                    <td data-title="'Name'" filter="{name:'text'}" sortable="'name'" ng-bind="user.name"></td>
                    <td data-title="'Email'" filter="{email:'text'}" sortable="'email'" ng-bind="user.email"></td>
                </tr>
            </table>
        </div>
    </div>

    <script src="{{ elixir('assets/js/all.js') }}"></script>

    <script>
        app.controller('UserController', ['NgTableParams', 'Restangular', function(NgTableParams, Restangular) {
            var vm = this;
            vm.userTable = new NgTableParams({}, {
                getData: function (params) {
                    var postParams = {
                        page: params.page(),
                        per_page: params.count()
                    };

                    var sorting = params.sorting();
                    if (Object.keys(sorting).length > 0) {
                        var key = Object.keys(sorting)[0];
                        postParams.sort = key;
                        postParams['order'] = sorting[key]
                    }

                    var filters = params.filter();
                    for (var i in filters) {
                        postParams[i] = filters[i];
                    }

                    return Restangular.all('api/users').getList(postParams).then(function (result) {
                        vm.userTable.total(result.meta.total);
                        vm.userTable.page(result.meta.current_page);
                        return result;
                    });
                }
            });
        }]);
    </script>
</body>
</html>
